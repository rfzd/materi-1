/// <reference types="cypress" />

const file = "Document.pdf";

describe("Privy", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://app.privy.id");
    cy.get('[name="user[privyId]"]').type(Cypress.env("privyId"));
    cy.get('[name="user[secret]"]').should("not.exist");
    cy.contains("CONTINUE").click();
    cy.get('[name="user[secret]"]').should("exist").and("be.visible");
    cy.get('[name="user[secret]"]').type(Cypress.env("password"));
    cy.contains("CONTINUE").click();
  });

  it("self sign", () => {
    cy.contains("button", "Upload Document").click();
    cy.contains("Self Sign").click();
    cy.get(".form-upload > [type=file]").attachFile(file);
    cy.get(".modal").should("be.visible");
    cy.contains("footer > button", "Upload").click();
    cy.contains("button", "Continue", { timeout: 1 * 10000 })
      .should("be.enabled")
      .click();
    cy.get("#step-document-1 > button", { timeout: 1 * 10000 })
      .should("contain.text", "Place Signature")
      .click();
    cy.contains("Done").should("be.enabled");
  });

  it("sign and share", () => {
    cy.contains("button", "Upload Document").click();
    cy.contains("Sign & Request").click();
    cy.get(".form-upload > [type=file]").attachFile(file);
    cy.get(".modal").should("be.visible");
    cy.contains("footer > button", "Upload").click();
    cy.contains("button", "Continue", { timeout: 1 * 10000 })
      .should("be.enabled")
      .click();
    cy.get("#step-document-1 > button", { timeout: 1 * 10000 })
      .should("contain.text", "Place Signature")
      .click();
    cy.contains("Done").should("be.enabled");
  });

  it("change image signature", () => {
    cy.contains("Change My Signature Image").click();
    cy.url().should("include", "/settings/signatures");
    cy.contains("button", "Add Signature").click();
    cy.get(".modal-dialog").should("be.visible");
    cy.contains("Image").click();
    cy.contains("Browse").click();
    cy.contains("button", "Cancel").click({ force: true });
  });
});
