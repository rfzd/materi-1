/// <reference types="cypress" />

describe("Basic test", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });
  it("Every basic element exist", () => {
    cy.visit("https://codedamn.com");

    // Get the DOM element containing the text
    cy.contains("Learning");

    // Get one or more DOM elements by selector.
    cy.get("div#root"); //another example w/ nested selector: cy.get(".border-l-1 > div > a");

    cy.contains("Learn Programming").should("have.text", "Learn Programming");

    // Get one or more DOM elements by data attribute. w/ click action
    cy.get("[data-bypassmenuclose=true]").click();

    // Set viewport to the given resolution.
    cy.viewport(1280, 720);
  });

  it("Every basic element exist on mobile", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });

  it("The web page load at, least", () => {
    cy.visit("https://codedamn.com");
  });

  it("Login page looks good", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    cy.contains("Sign in").click();
    cy.contains("Sign in to codedamn").should("exist");
    cy.contains("Forgot your password?").should("exist");
    cy.contains("Sign in with Google").should("exist");
    cy.contains("Sign in with GitHub").should("exist");
    // etc *if you wanna check all item is exist on the screen
  });

  it("The login page links work", () => {
    cy.contains("Forgot your password?").should("exist");
  });

  it("The links on the web page work", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    cy.contains("Pricing").click();
    cy.url().should("include", "/pricing");
    cy.go("back");
  });

  it.only("Login should work fine", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    cy.contains("Sign in").click();
    cy.get('[data-testid="username"]').type("admin");
    cy.contains("Sign in").click();
  });
});

// another way for getting item
// cy.get('[data-testid=test] > div').should('contain.text', 'the text');

// timeout for waiting (sometimes we need this for waiting component to load for pass test)
// cy.containts('setting blabla', { timeout: 1 * 1000 }).should('not.exist');
